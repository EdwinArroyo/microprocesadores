title memvideo (memvideo.asm)

;este programa usa la memorio de video para imprimir en pantalla un caracter

.model small
.stack 100h

.code
main proc
     mov ax,0b800h
     mov es,ax

     mov di,0f9eh         ;es:[di] direccion de memoria de video

     mov bl,113           ;atributo:azul fondo blanco 


     mov dl,'A'
     mov es:[di],dl      ;en posicion 00: caracter
     inc di
     mov es:[di],bl      ;en posicion 01: atributo

;exit
    mov ah,4Ch
    int 21h
main endp
end main